<?php
declare(strict_types=1);

use App\Domain\Models\User;
use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Test admin',
            'email' => 'admin',
            'password' => bcrypt('admin')
        ]);
    }
}
