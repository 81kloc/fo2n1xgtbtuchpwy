<?php
declare(strict_types=1);
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i <= 10; $i++) {

            $faker = \Faker\Factory::create();
            $publisher = new \App\Domain\Models\Publisher();
            $publisher->name = $faker->name;
            $publisher->save();
        }

        for ($i = 0; $i <= 100; $i++) {
            $faker = \Faker\Factory::create();
            $magazine = new \App\Domain\Models\Magazine();
            $magazine->name = $faker->company;
            $magazine->publisher_id = $faker->numberBetween(1, 10);
            $magazine->save();
        }

        $this->call(CreateUserSeeder::class);
    }
}
