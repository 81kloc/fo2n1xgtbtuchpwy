<?php
declare(strict_types=1);

Route::post('authorize', \App\Interfaces\Http\Api\User\Actions\AuthorizeAction::class)->name('authorize');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('publishers', \App\Interfaces\Http\Api\Publisher\Actions\PublisherListAction::class)->name('publishers');
    Route::get('magazine/search/{search?}/{id?}', \App\Interfaces\Http\Api\Magazine\Actions\MagazineSearchAction::class)->name('magazine.search');
    Route::get('magazine/{id}', \App\Interfaces\Http\Api\Magazine\Actions\MagazineShowAction::class)->name('magazine.show');
});


