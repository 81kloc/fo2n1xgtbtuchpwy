<?php
declare(strict_types=1);
namespace test\unit;

use Tests\TestCase;

class AuthorizeActionTest extends TestCase
{

    /** @test */
    public function it_has_token()
    {
        $this->assertEquals($this->authorize->getStatusCode(), 200);
    }

    /** @test */
    public function it_can_be_token()
    {

        $response = $this->call('POST', route('authorize'), array(
            'password' => 'test',
            'email' => 'test',
        ));

        $this->assertEquals($response->getStatusCode(), 400);
    }
}
