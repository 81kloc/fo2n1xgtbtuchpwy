<?php
declare(strict_types=1);
namespace test\unit;

use Tests\TestCase;

class PublisherListActionTest extends TestCase
{
    /** @test */
    public function it_has_publisher()
    {
        $response = $this->call('GET', route('publishers'))->header( 'Authorization', json_decode($this->authorize->content())->token);

        $this->assertIsObject(json_decode($response->content()));
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
