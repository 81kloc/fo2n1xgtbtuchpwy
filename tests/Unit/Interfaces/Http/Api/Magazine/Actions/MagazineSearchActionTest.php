<?php
declare(strict_types=1);

namespace Tests\Unit\Interfaces\Http\Api\Magazine\Actions;

use Tests\TestCase;

class MagazineSearchActionTest extends TestCase
{
    /** @test */
    public function it_has_magazine_search()
    {
        $response = $this->call('GET', route('magazine.search', ['search' => 'a', 'id' => null]))->header( 'Authorization', json_decode($this->authorize->content())->token);

        $this->assertIsObject(json_decode($response->content()));
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
