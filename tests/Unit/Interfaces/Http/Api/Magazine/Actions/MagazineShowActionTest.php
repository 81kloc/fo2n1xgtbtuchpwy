<?php
declare(strict_types=1);

namespace Tests\Unit\Interfaces\Http\Api\Magazine\Actions;

use Tests\TestCase;

class MagazineShowActionTest extends TestCase
{
    /** @test */
    public function it_has_magazine()
    {
        $response = $this->call('GET', route('magazine.show', ['id' => 1]))->header( 'Authorization', json_decode($this->authorize->content())->token);

        $this->assertIsObject(json_decode($response->content()));
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
