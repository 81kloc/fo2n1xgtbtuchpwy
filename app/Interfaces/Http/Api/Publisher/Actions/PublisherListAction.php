<?php
declare(strict_types=1);

namespace App\Interfaces\Http\Api\Publisher\Actions;

use App\Infrastructure\Commands\Publisher\GetListPublisherCommand;
use App\Interfaces\Http\Controllers\BaseAction;

class PublisherListAction extends BaseAction
{
    /**
     * @var GetListPublisherCommand
     */
    private $command;

    /**
     * PublisherListAction constructor.
     * @param GetListPublisherCommand $command
     */
    public function __construct(GetListPublisherCommand $command)
    {
        $this->command = $command;
    }

    public function __invoke()
    {
        return response()->json([$this->command->execute()], 200);
    }
}
