<?php
declare(strict_types=1);

namespace App\Interfaces\Http\Api\Magazine\Actions;

use App\Infrastructure\Commands\Magazine\SearchMagazineCommand;
use App\Interfaces\Http\Controllers\BaseAction;

class MagazineSearchAction extends BaseAction
{
    /**
     * @var SearchMagazineCommand
     */
    private $command;

    /**
     * MagazineSearchAction constructor.
     * @param SearchMagazineCommand $command
     */
    public function __construct(SearchMagazineCommand $command)
    {
        $this->command = $command;
    }

    public function __invoke(?string $search = null, ?int $id = null)
    {
        return response()->json([$this->command->execute($search, $id)], 200);
    }
}
