<?php
declare(strict_types=1);

namespace App\Interfaces\Http\Api\Magazine\Actions;

use App\Infrastructure\Commands\Magazine\GetByIdMagazineCommand;
use App\Interfaces\Http\Controllers\BaseAction;

class MagazineShowAction extends BaseAction
{
    /**
     * @var GetByIdMagazineCommand
     */
    private $command;

    /**
     * MagazineAction constructor.
     * @param GetByIdMagazineCommand $command
     */
    public function __construct(GetByIdMagazineCommand $command)
    {
        $this->command = $command;
    }

    public function __invoke(int $id)
    {
        return response()->json([$this->command->execute($id)], 200);
    }
}
