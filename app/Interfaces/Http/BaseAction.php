<?php
declare(strict_types=1);

namespace App\Interfaces\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class BaseAction extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
