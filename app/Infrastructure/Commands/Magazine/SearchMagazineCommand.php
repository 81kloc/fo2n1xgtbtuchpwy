<?php
declare(strict_types=1);

namespace App\Infrastructure\Commands\Magazine;

use App\Domain\Contracts\MagazineContract;
use App\Infrastructure\Commands\AbstractCommand;

class SearchMagazineCommand extends AbstractCommand
{
    /**
     * @var MagazineContract
     */
    private $contract;

    /**
     * SearchMagazineCommand constructor.
     * @param MagazineContract $contract
     */
    public function __construct(MagazineContract $contract)
    {
        $this->contract = $contract;
    }

    public function execute(?string $search, ?int $id)
    {
        return $this->contract->search($search, $id);
    }
}
