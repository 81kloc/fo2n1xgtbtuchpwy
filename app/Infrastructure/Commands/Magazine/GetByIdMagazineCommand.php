<?php
declare(strict_types=1);

namespace App\Infrastructure\Commands\Magazine;

use App\Domain\Contracts\MagazineContract;
use App\Infrastructure\Commands\AbstractCommand;

class GetByIdMagazineCommand extends AbstractCommand
{
    /**
     * @var MagazineContract
     */
    private $contract;

    /**
     * GetByIdMagazineCommand constructor.
     * @param MagazineContract $contract
     */
    public function __construct(MagazineContract $contract)
    {
        $this->contract = $contract;
    }

    public function execute(int $id)
    {
        return $this->contract->getById($id);
    }
}
