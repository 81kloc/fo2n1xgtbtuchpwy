<?php
declare(strict_types=1);

namespace App\Infrastructure\Commands\Publisher;

use App\Domain\Contracts\PublisherContract;
use App\Infrastructure\Commands\AbstractCommand;

class GetListPublisherCommand extends AbstractCommand
{
    /**
     * @var PublisherContract
     */
    private $contract;

    /**
     * GetListPublisherCommand constructor.
     * @param PublisherContract $contract
     */
    public function __construct(PublisherContract $contract)
    {
        $this->contract = $contract;
    }

    public function execute()
    {
        return $this->contract->list();
    }
}
