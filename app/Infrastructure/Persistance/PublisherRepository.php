<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\PublisherContract;
use App\Domain\Models\Publisher;
use Illuminate\Database\Eloquent\Collection;

class PublisherRepository implements PublisherContract
{
    public function list(): ?Collection
    {
        return Publisher::all();
    }
}
