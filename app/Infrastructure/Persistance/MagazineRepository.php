<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\MagazineContract;
use App\Domain\Models\Magazine;
use Illuminate\Pagination\LengthAwarePaginator;

class MagazineRepository implements MagazineContract
{
    public function search(?string $search = null, ?int $id = null, int $page = 0): ?LengthAwarePaginator
    {

        return Magazine::when($search !== null,
            function($q) use ($search){
                return $q->where('name','like', '%'.$search.'%');
            }
        )->when($id !== null,
            function($q) use ($id){
                return $q->where('id', $id);
            }
        )->paginate(15);
    }

    public function getById(int $id): ?Magazine
    {
        return Magazine::find($id);
    }
}
