<?php
declare(strict_types=1);

namespace App\Domain\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface PublisherContract
{
    public function list(): ?Collection;
}
