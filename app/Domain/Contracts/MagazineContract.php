<?php
declare(strict_types=1);

namespace App\Domain\Contracts;

use App\Domain\Models\Magazine;
use Illuminate\Pagination\LengthAwarePaginator;

interface MagazineContract
{
    public function search(?string $search = null, ?int $id = null): ? LengthAwarePaginator;

    public function getById(int $id): ?Magazine;
}
