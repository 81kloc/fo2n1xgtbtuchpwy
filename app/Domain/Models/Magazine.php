<?php
declare(strict_types=1);

namespace App\Domain\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    use Cachable;

    protected $fillable = ['name'];
}
