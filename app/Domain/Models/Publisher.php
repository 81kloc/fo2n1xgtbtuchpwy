<?php
declare(strict_types=1);

namespace App\Domain\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    use Cachable;

    protected $fillable = ['name', 'publisher_id'];

    public function magazine(){
        return $this->belongsTo('App\Models\Magazine');
    }
}
