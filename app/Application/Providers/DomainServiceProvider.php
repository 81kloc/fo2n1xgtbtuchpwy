<?php
declare(strict_types=1);

namespace App\Application\Providers;

use App\Domain\Contracts\MagazineContract;
use App\Domain\Contracts\PublisherContract;
use App\Infrastructure\Persistance\MagazineRepository;
use App\Infrastructure\Persistance\PublisherRepository;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap domain application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register domain application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PublisherContract::class, PublisherRepository::class);
        $this->app->bind(MagazineContract::class, MagazineRepository::class);
    }
}
